/**
  ******************************************************************************
  * @file    Project/STM32F10x_StdPeriph_Template/main.c
  * @author  MCD Application Team
  * @version V3.5.0
  * @date    08-April-2011
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f10x.h"
#include "stm32_eval.h"
#include <stdio.h>

#include "deca_device_api.h"
#include "deca_regs.h"
#include "deca_sleep.h"
//#include "lcd.h"
#include "port.h"
#include "lcd_oled.h"
#include "frame_header.h"
#include "bp_filter.h"
#include "common_header.h"
#include "bphero_uwb.h"
#include "dwm1000_timestamp.h"
/* Example application name and version to display on LCD screen. */

struct time_timestamp tx_node[MAX_TARGET_NODE];
static unsigned char distance_seqnum = 0;

static void Handle_TimeStamp(void);
static srd_msg_dsss *msg_f_recv ;
//static void final_msg_set_ts(uint8 *ts_field, uint64 ts);
/******************************************************************************************************************
********************* NOTES on DW (MP) features/options ***********************************************************
*******************************************************************************************************************/
//#define BP_UWB_INIT 1
//#define BP_UWB_BROADCAST 2
//#define BP_UWB_PAIR 3
//#define BP_UWB_TRAMSMIT 4


uint8 TX_ANTHOR_STATUS;


uint16 Tag_Address[10];//max 10 tag allowed
uint8 Tag_Index = 0;
#define BroadCastOverTime 10 //over time 10ms,normally measure ranging time about 5ms
uint32 TimeBroadCastComplete = 0;
uint8 BroadCastComplete = 0;
uint16 Target_Address = 0x0000;
bool bFindTarget = false;
uint8 Pair_retry = 0;
void FindTarget(srd_msg_dsss *bpmsg)
{
    uint32 distance = 0;
    char lcd_display_str[100]= {'0'};

    if(bpmsg->messageData[2]  == 'V')
    {
        distance = bpmsg->messageData[3]*100 + bpmsg->messageData[4];//all in cm
        if( distance > 50 && distance <120)
        {
            Target_Address = (bpmsg->sourceAddr[0] )|(bpmsg->sourceAddr[1] <<8);
            sprintf(lcd_display_str, "dis:%d", distance);
            OLED_ShowString(0,6,(uint8_t *)lcd_display_str);
            TX_ANTHOR_STATUS = BP_UWB_PAIR;
            bFindTarget = true;
            Pair_retry = 0;
            return;
        }
    }
    else
    {
        bFindTarget = false;
    }
    bFindTarget = false;
}

extern unsigned char USART1QRecvBuffer_Temp[32];//temp
extern unsigned char USART1_LEN;
extern unsigned char USART1_OK;


void BPhero_ControlMesg_Tansmit(unsigned char *DatatoAnhor,unsigned char dataLength)
{
    int Data_Index = 1,Data_Length = 0;

    msg_f_send.destAddr[0] = Target_Address&0xFF;
    msg_f_send.destAddr[1] = (Target_Address>>8) &0xFF;

    msg_f_send.seqNum = 0;
    msg_f_send.messageData[0]='C';
    Data_Length  = (dataLength+1 + FRAME_CRTL_AND_ADDRESS_S + FRAME_CRC);//modify data length

    while(dataLength--)
    {
        msg_f_send.messageData[Data_Index++] = *DatatoAnhor++;
    }
    dwt_forcetrxoff();

    //  Data_Length  = (Data_Index + FRAME_CRTL_AND_ADDRESS_S + FRAME_CRC);//modify data length
    dwt_writetxdata(Data_Length, (uint8 *)&msg_f_send, 0) ; // write the frame data
    dwt_writetxfctrl(Data_Length, 0);
    /* Start transmission. */
    dwt_starttx(DWT_START_TX_IMMEDIATE);
    //MUST WAIT!!!!!
    while (!((status_reg = dwt_read32bitreg(SYS_STATUS_ID)) & (SYS_STATUS_TXFRS)))
    { };
    dwt_write32bitreg(SYS_STATUS_ID, SYS_STATUS_TXFRS|SYS_STATUS_RXFCG);

}

/* Private functions ---------------------------------------------------------*/
void Tx_Simple_Rx_Callback()
{
    int i=0;
    for (i = 0 ; i < FRAME_LEN_MAX; i++ )
    {
        rx_buffer[i] = '\0';
    }
    /* Activate reception immediately. See NOTE 2 below. */
    dwt_enableframefilter(DWT_FF_RSVD_EN);//disable recevie
    status_reg = dwt_read32bitreg(SYS_STATUS_ID);

    if (status_reg & SYS_STATUS_RXFCG)
    {
        /* A frame has been received, copy it to our local buffer. */
        frame_len = dwt_read32bitreg(RX_FINFO_ID) & RX_FINFO_RXFL_MASK_1023;
        if (frame_len <= FRAME_LEN_MAX)
        {
            dwt_readrxdata(rx_buffer, frame_len, 0);
            msg_f_recv = (srd_msg_dsss*)rx_buffer;
            msg_f_send.destAddr[0] = msg_f_recv->sourceAddr[0];
            msg_f_send.destAddr[1] = msg_f_recv->sourceAddr[1];

            msg_f_send.seqNum = msg_f_recv->seqNum;

            switch(msg_f_recv->messageData[0])
            {
                case 'b'://report BroadCast message,tag report address
                    Tag_Address[Tag_Index++] = (msg_f_recv->sourceAddr[0])| ((msg_f_recv->sourceAddr[1])<<8);
                    if(Tag_Index == 5)
                        BroadCastComplete = 1;
                    dwt_enableframefilter(DWT_FF_DATA_EN);
                    dwt_setrxtimeout(0);
                    dwt_rxenable(0);
                    break;
                case 'd'://distance
                    tx_node[msg_f_recv->messageData[1]].tx_ts[0] = get_tx_timestamp_u64();
                    tx_node[msg_f_recv->messageData[1]].rx_ts[0] = get_rx_timestamp_u64();
                    FindTarget(msg_f_recv);
                    break;
                //more message, distance
                //status =init
                case 'p'://parie ack message
                    if((msg_f_recv->sourceAddr[0])| ((msg_f_recv->sourceAddr[1])<<8)== Target_Address)
                    {
                        TX_ANTHOR_STATUS = BP_UWB_TRAMSMIT;
                    }
                    break;
                case 's': //serial data
                    //remove crc
                    rx_buffer[frame_len]=0x00;
                    rx_buffer[frame_len-1]=0x00;
                    rx_buffer[frame_len-2]=0x00;

                    //e(end) serial transanction
                    if(msg_f_recv->messageData[1] == 'e')
                    {
                        TX_ANTHOR_STATUS = BP_UWB_INIT;
                        Target_Address = 0x0000;
                        bFindTarget = false;
                        Pair_retry = 0;
                        // continue;
                    }
                    else
                    {
                        USART1DispFun((unsigned char *)(&(msg_f_recv->messageData[1])));
                        if(USART1_OK == 1 &&  USART1_LEN >0)
                        {
                            BPhero_ControlMesg_Tansmit(USART1QRecvBuffer_Temp,USART1_LEN);
                            USART1_OK =0;
                        }
                        //USART1DispFun("HELLO!");
                        dwt_enableframefilter(DWT_FF_DATA_EN);
                        dwt_setrxtimeout(0);
                        dwt_rxenable(0);
                    }
                    break;
                default:
                    break;
            }

        }
    }
    else
    {
        dwt_write32bitreg(SYS_STATUS_ID, (SYS_STATUS_RXFCG | SYS_STATUS_ALL_RX_ERR));
        //enable recive again
        dwt_enableframefilter(DWT_FF_DATA_EN);
        dwt_setdelayedtrxtime(dwt_readsystimestamphi32() + 0x100000/2);
        dwt_rxenable(0);
    }
}

static void Handle_TimeStamp(void)
{
}

static void Handle_LCD(void)
{
}

#define TAG_ADDRESS_BASE 0x010A
unsigned long time1,time2;


void BPhero_UWB_Tag_Statistic()
{
    int seqnum = 0;

    Tag_Index = 0;

    msg_f_send.destAddr[0] = 0xFF;
    msg_f_send.destAddr[1] = 0xFF;
    msg_f_send.seqNum = seqnum;
    msg_f_send.messageData[0]='B';

    //TODO recompute psduLength
    dwt_writetxdata(psduLength, (uint8 *)&msg_f_send, 0) ; // write the frame data
    dwt_writetxfctrl(psduLength, 0);
    /* Start transmission. */
    dwt_starttx(DWT_START_TX_IMMEDIATE);

    //Enable RX
    dwt_setrxtimeout(0);
    dwt_rxenable(0);

    //wait for about 10ms
    TimeBroadCastComplete = portGetTickCnt();
    BroadCastComplete = 0;
    while((BroadCastComplete ==0)&&(portGetTickCnt()<(TimeBroadCastComplete+BroadCastOverTime)));

    if(Tag_Index != 0)
        TX_ANTHOR_STATUS = BP_UWB_BROADCAST;
    //disable recive
    dwt_forcetrxoff();
}



void BPhero_Distance_Measure()
{
    char lcd_display_str[100]= {'0'};
    int address = 0;
    uint8 index = 0;
    for( index= 0; index < Tag_Index ; index++)
    {
        msg_f_send.destAddr[0] = Tag_Address[index] &0xFF;
        msg_f_send.destAddr[1] = (Tag_Address[index]>>8) &0xFF;
        /* Write all timestamps in the final message. See NOTE 10 below. */
        final_msg_set_ts(&msg_f_send.messageData[FIRST_TX],  tx_node[index].tx_ts[0] );
        final_msg_set_ts(&msg_f_send.messageData[FIRST_RX],  tx_node[index].rx_ts[0] );

        msg_f_send.seqNum = distance_seqnum;
        msg_f_send.messageData[0]='D';
        msg_f_send.messageData[1]=index;

        dwt_writetxdata(psduLength, (uint8 *)&msg_f_send, 0) ; // write the frame data
        dwt_writetxfctrl(psduLength, 0);
        dwt_starttx(DWT_START_TX_IMMEDIATE);

        dwt_enableframefilter(DWT_FF_DATA_EN);
        dwt_rxenable(0);

        //wait 10ms
        TimeBroadCastComplete = portGetTickCnt();
        BroadCastComplete = 0;
        while((BroadCastComplete ==0)&&(portGetTickCnt()<(TimeBroadCastComplete+BroadCastOverTime)));
        dwt_forcetrxoff();
        /* Clear good RX frame event in the DW1000 status register. */
        if(++distance_seqnum == 256)
            distance_seqnum = 0;
    }
}

void BPhero_Pair_Request()
{
    msg_f_send.destAddr[0] = Target_Address&0xFF;
    msg_f_send.destAddr[1] = (Target_Address>>8) &0xFF;

    msg_f_send.seqNum = 0;
    msg_f_send.messageData[0]='P';

    dwt_writetxdata(psduLength, (uint8 *)&msg_f_send, 0) ; // write the frame data
    dwt_writetxfctrl(psduLength, 0);
    dwt_starttx(DWT_START_TX_IMMEDIATE);

    dwt_enableframefilter(DWT_FF_DATA_EN);
    dwt_rxenable(0);
    //wait 10ms
    TimeBroadCastComplete = portGetTickCnt();
    BroadCastComplete = 0;
    while((BroadCastComplete ==0)&&(portGetTickCnt()<(TimeBroadCastComplete+BroadCastOverTime)));
    dwt_forcetrxoff();
}

void BPhero_Data_Tansmit(void)
{
    //TODO check if rx already enable
    dwt_enableframefilter(DWT_FF_DATA_EN);
    dwt_rxenable(0);
    dwt_setrxtimeout(0);
}

uint8 Last_Status = BP_UWB_INIT;
int tx_main(void)
{
    char lcd_display_str[100]= {'0'};
    TIM4_Int_Init_timeout(500);//����LCDˢ����ʾ
    /* Configure DW1000. See NOTE 2 below. */
    USART1DispFun("BPHER-UWB NODE-TX!\r\n");
    sprintf(lcd_display_str, "UWB-TX:0x%04X", SHORT_ADDR);
    OLED_ShowString(0,0,(uint8_t *)lcd_display_str);

    bphero_setcallbacks(Tx_Simple_Rx_Callback);
    TX_ANTHOR_STATUS = BP_UWB_INIT;
    /* Infinite loop */
    while(1)
    {
        IWDG_Feed();
        if(TX_ANTHOR_STATUS ==  BP_UWB_INIT)
        {
            OLED_ShowString(0,2,"            ");
            OLED_ShowString(0,2,"UWB_INIT");
            BPhero_UWB_Tag_Statistic();
        }
        if(TX_ANTHOR_STATUS ==  BP_UWB_BROADCAST)
        {
            if(Last_Status !=TX_ANTHOR_STATUS)
            {
                OLED_ShowString(0,2,"            ");
                OLED_ShowString(0,2,"UWB_BROADCAST");
            }
            Last_Status = TX_ANTHOR_STATUS;
            BPhero_Distance_Measure();
        }

        if(bFindTarget == true)
        {
        }
        if(TX_ANTHOR_STATUS ==  BP_UWB_PAIR)
        {
            if(Last_Status !=TX_ANTHOR_STATUS)
            {
                OLED_ShowString(0,2,"            ");
                OLED_ShowString(0,2,"UWB_PAIR");
                sprintf(lcd_display_str, "TARGET:0X%04X", Target_Address);
                OLED_ShowString(0,4,(uint8_t *)lcd_display_str);
            }
            Last_Status = TX_ANTHOR_STATUS;

            BPhero_Pair_Request();
            if(++Pair_retry>5)
            {
                TX_ANTHOR_STATUS =  BP_UWB_INIT;
                bFindTarget = false;
                dwt_forcetrxoff();
            }
        }
        if(TX_ANTHOR_STATUS ==  BP_UWB_TRAMSMIT)
        {
            //send reset message
            if(Last_Status !=TX_ANTHOR_STATUS)
            {
                OLED_ShowString(0,2,"            ");
                OLED_ShowString(0,2,"UWB_TRAMSMIT");

                BPhero_Data_Tansmit();
                Last_Status = TX_ANTHOR_STATUS;
            }
        }
    }

}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
    }
}
#endif

/**
  * @}
  */
/*****************************************************************************************************************************************************
 * NOTES:
 *
 * 1. In this example, maximum frame length is set to 127 bytes which is 802.15.4 UWB standard maximum frame length. DW1000 supports an extended
 *    frame length (up to 1023 bytes long) mode which is not used in this example.
 * 2. Manual reception activation is performed here but DW1000 offers several features that can be used to handle more complex scenarios or to
 *    optimise system's overall performance (e.g. timeout after a given time, automatic re-enabling of reception in case of errors, etc.).
 * 3. We use polled mode of operation here to keep the example as simple as possible but RXFCG and error/timeout status events can be used to generate
 *    interrupts. Please refer to DW1000 User Manual for more details on "interrupts".
 * 4. The user is referred to DecaRanging ARM application (distributed with EVK1000 product) for additional practical example of usage, and to the
 *    DW1000 API Guide for more details on the DW1000 driver functions.
 ****************************************************************************************************************************************************/
/**
  * @}
  */
/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
