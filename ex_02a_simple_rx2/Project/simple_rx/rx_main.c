/**
  ******************************************************************************
  * @file    Project/STM32F10x_StdPeriph_Template/main.c
  * @author  MCD Application Team
  * @version V3.5.0
  * @date    08-April-2011
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f10x.h"
#include "stm32_eval.h"
#include <stdio.h>

#include "deca_device_api.h"
#include "deca_regs.h"
#include "deca_sleep.h"
//#include "lcd.h"
#include "port.h"
#include "lcd_oled.h"
#include "frame_header.h"
#include "bp_filter.h"
#include "common_header.h"
#include "bphero_uwb.h"
#include "dwm1000_timestamp.h"
/* Example application name and version to display on LCD screen. */
static void Handle_TimeStamp(void);

//static void final_msg_set_ts(uint8 *ts_field, uint64 ts);
/******************************************************************************************************************
********************* NOTES on DW (MP) features/options ***********************************************************
*******************************************************************************************************************/

/* Private functions ---------------------------------------------------------*/
#define MAX_ANTHOR_NODE 4
struct distance_struct
{
    double rx_distance;
    uint8 rx_dis_index ;
    struct time_timestamp tx_node;
    struct time_timestamp rx_node;
    bool present;
    int count;
} bphero_distance[MAX_ANTHOR_NODE];

uint8 RX_ANTHOR_STATUS;


static int fresh_count = 0;
static double distance_temp;

extern int lcd_show;
static srd_msg_dsss *msg_f ;
static bool recevie_massge = false;
//static uint32 TimeBroadCastComplete = 0;
uint16 Anthor_Target_Address = 0x0000;

void Simple_Rx_Callback()
{
    int i=0;
    for (i = 0 ; i < FRAME_LEN_MAX; i++ )
    {
        rx_buffer[i] = '\0';
    }
    /* Activate reception immediately. See NOTE 2 below. */
    dwt_enableframefilter(DWT_FF_RSVD_EN);//disable recevie
    status_reg = dwt_read32bitreg(SYS_STATUS_ID);

    if (status_reg & SYS_STATUS_RXFCG)
    {
        /* A frame has been received, copy it to our local buffer. */
        frame_len = dwt_read32bitreg(RX_FINFO_ID) & RX_FINFO_RXFL_MASK_1023;
        if (frame_len <= FRAME_LEN_MAX)
        {
            dwt_readrxdata(rx_buffer, frame_len, 0);
            msg_f = (srd_msg_dsss*)rx_buffer;
            msg_f_send.destAddr[0] = msg_f->sourceAddr[0];
            msg_f_send.destAddr[1] = msg_f->sourceAddr[1];

            msg_f_send.seqNum = msg_f->seqNum;

            switch(msg_f->messageData[0])
            {
                case 'B'://broadcast message
                    msg_f_send.messageData[0]='b';
                    Delay_us(800*(SHORT_ADDR&0xFF));//TODO and random number
                    break;

                case 'D'://distance
                    msg_f_send.messageData[0]='d';
                    msg_f_send.messageData[1]=msg_f->messageData[1];
                    if( bphero_distance[msg_f->sourceAddr[0]].count == 10)
                    {
                        msg_f_send.messageData[2] = 'V';
                        int distance0  = (int)(bphero_distance[msg_f->sourceAddr[0]].rx_distance*100); //distance 0
                        msg_f_send.messageData[3]= (uint8)(distance0/100);//整数m
                        msg_f_send.messageData[4] = (uint8)(distance0%100);//小数cm
                    }
                    else
                    {
                        msg_f_send.messageData[2] = 'N';
                    }
                    break;
                case 'P'://parie
                    msg_f_send.messageData[0]='p';
                    Anthor_Target_Address = (msg_f->sourceAddr[0] )|(msg_f->sourceAddr[1] <<8);
                    RX_ANTHOR_STATUS = BP_UWB_TRAMSMIT;
					USART1DispFun("Parid\n");
                    break;
				case 'C'://control message				
                    //remove crc
                    rx_buffer[frame_len]=0x00;
                    rx_buffer[frame_len-1]=0x00;
                    rx_buffer[frame_len-2]=0x00;
                    USART1DispFun((unsigned char *)(&(msg_f->messageData[1])));
					break;
                default:
                    break;
            }
            dwt_writetxdata(psduLength, (uint8 *)&msg_f_send, 0) ; // write the frame data
            dwt_writetxfctrl(psduLength, 0);
            /* Start transmission. */
            dwt_starttx(DWT_START_TX_IMMEDIATE);
            //MUST WAIT!!!!!
            while (!((status_reg = dwt_read32bitreg(SYS_STATUS_ID)) & (SYS_STATUS_TXFRS)))
            { };
            dwt_write32bitreg(SYS_STATUS_ID, SYS_STATUS_TXFRS|SYS_STATUS_RXFCG);

            if(msg_f->messageData[0] == 'D')
            {
                Handle_TimeStamp();
            }
            //recevie_massge = true;
            //enable recive again
            dwt_enableframefilter(DWT_FF_DATA_EN);
            /* Compute final message transmission time. See NOTE 9 below. */
            dwt_setrxtimeout(0);
            dwt_rxenable(0);
        }
    }
    else
    {
        dwt_write32bitreg(SYS_STATUS_ID, (SYS_STATUS_RXFCG | SYS_STATUS_ALL_RX_ERR));
        //enable recive again
        dwt_enableframefilter(DWT_FF_DATA_EN);
        dwt_setdelayedtrxtime(dwt_readsystimestamphi32() + 0x100000/2);
        dwt_rxenable(0);
    }
}

static void Handle_TimeStamp(void)
{
    fresh_count++;
    bphero_distance[msg_f->sourceAddr[0]].present = true;
    bphero_distance[msg_f->sourceAddr[0]].rx_node.tx_ts[0] = bphero_distance[msg_f->sourceAddr[0]].rx_node.tx_ts[1];
    bphero_distance[msg_f->sourceAddr[0]].rx_node.rx_ts[0] = bphero_distance[msg_f->sourceAddr[0]].rx_node.rx_ts[1];
    bphero_distance[msg_f->sourceAddr[0]].rx_node.tx_ts[1] = get_tx_timestamp_u64();
    bphero_distance[msg_f->sourceAddr[0]].rx_node.rx_ts[1] = get_rx_timestamp_u64();

    final_msg_get_ts((uint8 *)&(msg_f->messageData[FIRST_TX]), &bphero_distance[msg_f->sourceAddr[0]].tx_node.tx_ts[0]);
    final_msg_get_ts((uint8 *)&(msg_f->messageData[FIRST_RX]), &bphero_distance[msg_f->sourceAddr[0]].tx_node.rx_ts[0]);
    {
        distance_temp = (((bphero_distance[msg_f->sourceAddr[0]].tx_node.rx_ts[0] -bphero_distance[msg_f->sourceAddr[0]].tx_node.tx_ts[0] ) - (bphero_distance[msg_f->sourceAddr[0]].rx_node.tx_ts[0] - bphero_distance[msg_f->sourceAddr[0]].rx_node.rx_ts[0] )) / 2.0) * DWT_TIME_UNITS * SPEED_OF_LIGHT;
        if(distance_temp<80 && distance_temp>0.1)
        {
            bphero_distance[msg_f->sourceAddr[0]].count++;
            //  bphero_distance[msg_f->sourceAddr[0]].rx_distance = (double)filter((int)(distance_temp*1000),msg_f->sourceAddr[0])/1000;
            bphero_distance[msg_f->sourceAddr[0]].rx_distance = distance_temp;
        }
    }
}

static void Handle_LCD(void)
{
    char lcd_display_str[100]= {'0'};

    sprintf(lcd_display_str, "%3.2f-%02X",bphero_distance[0].rx_distance,bphero_distance[0].count);
    OLED_ShowString(0,2,(uint8_t *)lcd_display_str);
    sprintf(lcd_display_str, "%3.2f-%02X   ",bphero_distance[1].rx_distance,bphero_distance[1].count);
    OLED_ShowString(0,4,(uint8_t *)lcd_display_str);
    sprintf(lcd_display_str, "%3.2f-%02X   ",bphero_distance[2].rx_distance,bphero_distance[2].count);
    OLED_ShowString(0,6,(uint8_t *)lcd_display_str);
    bphero_distance[0].count =0;
    bphero_distance[1].count =0 ;
    bphero_distance[2].count =0 ;
}


void Anthor_BPhero_Data_Tansmit(unsigned char *DatatoAnhor,unsigned char dataLength)
{
    int Data_Index = 1,Data_Length = 0;

    msg_f_send.destAddr[0] = Anthor_Target_Address&0xFF;
    msg_f_send.destAddr[1] = (Anthor_Target_Address>>8) &0xFF;

    msg_f_send.seqNum = 0;
    msg_f_send.messageData[0]='s';

//    msg_f_send.messageData[1]='B';
//    msg_f_send.messageData[2]='P';
//    msg_f_send.messageData[3]='U';
//    msg_f_send.messageData[4]='W';
//    msg_f_send.messageData[5]='B';
    Data_Length  = (dataLength+1 + FRAME_CRTL_AND_ADDRESS_S + FRAME_CRC);//modify data length

    while(dataLength--)
    {
        msg_f_send.messageData[Data_Index++] = *DatatoAnhor++;
    }
    dwt_forcetrxoff();

    //  Data_Length  = (Data_Index + FRAME_CRTL_AND_ADDRESS_S + FRAME_CRC);//modify data length
    dwt_writetxdata(Data_Length, (uint8 *)&msg_f_send, 0) ; // write the frame data
    dwt_writetxfctrl(Data_Length, 0);
    /* Start transmission. */
    dwt_starttx(DWT_START_TX_IMMEDIATE);
    //MUST WAIT!!!!!
    while (!((status_reg = dwt_read32bitreg(SYS_STATUS_ID)) & (SYS_STATUS_TXFRS)))
    { };
    dwt_write32bitreg(SYS_STATUS_ID, SYS_STATUS_TXFRS|SYS_STATUS_RXFCG);

	//rx mode,recive control message
	
	dwt_enableframefilter(DWT_FF_DATA_EN);
	/* Compute final message transmission time. See NOTE 9 below. */
	dwt_setrxtimeout(0);
	dwt_rxenable(0);

}


extern unsigned char USART1QRecvBuffer_Temp[32];//temp
extern unsigned char USART1_LEN;
extern unsigned char USART1_OK;
int rx_main(void)
{
    char lcd_display_str[100]= {'0'};
    TIM4_Int_Init_timeout(500);//用于LCD刷新显示
    USART1DispFun("BPHER-UWB NODE-RX!\r\n");
    sprintf(lcd_display_str, "UWB-RX:0x%04X", SHORT_ADDR);
    OLED_ShowString(0,0,(uint8_t *)lcd_display_str);

    bphero_distance[0].rx_distance=0;
    bphero_distance[1].rx_distance=0;
    bphero_distance[2].rx_distance=0;
    dwt_setrxtimeout(0);
    dwt_enableframefilter(DWT_FF_DATA_EN);
    dwt_rxenable(0);
    bphero_setcallbacks(Simple_Rx_Callback);
    RX_ANTHOR_STATUS = BP_UWB_INIT;
    while (1)
    {
        IWDG_Feed();
        if(recevie_massge == true)
        {
            //Handle_TimeStamp();
            recevie_massge =false;
        }
        if(lcd_show == true)
        {

            Handle_LCD();
            lcd_show = false;
        }
        if(RX_ANTHOR_STATUS == BP_UWB_TRAMSMIT)
        {
            sprintf(lcd_display_str, "TARGET:0X%04X", Anthor_Target_Address);
            OLED_ShowString(0,6,(uint8_t *)lcd_display_str);

            if(USART1_OK ==1 )
            {
                Anthor_BPhero_Data_Tansmit(USART1QRecvBuffer_Temp,USART1_LEN);
                USART1_OK =0;
            }

            //send the serial data to anthor
            //Anthor_BPhero_Data_Tansmit("BHHERO-UWB!\r",sizeof("BHHERO-UWB!\r"));
            // Delay_us(50);
        }
    }
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
    }
}
#endif

/**
  * @}
  */
/*****************************************************************************************************************************************************
 * NOTES:
 *
 * 1. In this example, maximum frame length is set to 127 bytes which is 802.15.4 UWB standard maximum frame length. DW1000 supports an extended
 *    frame length (up to 1023 bytes long) mode which is not used in this example.
 * 2. Manual reception activation is performed here but DW1000 offers several features that can be used to handle more complex scenarios or to
 *    optimise system's overall performance (e.g. timeout after a given time, automatic re-enabling of reception in case of errors, etc.).
 * 3. We use polled mode of operation here to keep the example as simple as possible but RXFCG and error/timeout status events can be used to generate
 *    interrupts. Please refer to DW1000 User Manual for more details on "interrupts".
 * 4. The user is referred to DecaRanging ARM application (distributed with EVK1000 product) for additional practical example of usage, and to the
 *    DW1000 API Guide for more details on the DW1000 driver functions.
 ****************************************************************************************************************************************************/
/**
  * @}
  */
/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
