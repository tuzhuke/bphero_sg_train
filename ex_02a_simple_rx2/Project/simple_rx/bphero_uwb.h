#ifndef BPHERO_UWB_H
#define BPHERO_UWB_H

#include "frame_header.h"
#include "common_header.h"

#ifndef SHORT_ADDR
#define SHORT_ADDR 0x0200
#endif
extern int psduLength ;
extern srd_msg_dsss msg_f_send ; // ranging message frame with 16-bit addresses

#ifndef SPEED_OF_LIGHT
#define SPEED_OF_LIGHT      (299702547.0)     // in m/s in air
#endif
/* Buffer to store received frame. See NOTE 1 below. */
#ifndef FRAME_LEN_MAX
	#define FRAME_LEN_MAX 127
#endif

#ifndef TX_ANT_DLY
#define TX_ANT_DLY 0
#endif

#ifndef RX_ANT_DLY
#define RX_ANT_DLY 32950
#endif

#define BP_UWB_INIT 1
#define BP_UWB_BROADCAST 2
#define BP_UWB_PAIR 3
#define BP_UWB_TRAMSMIT 4

extern uint8 rx_buffer[FRAME_LEN_MAX];
/* Hold copy of status register state here for reference, so reader can examine it at a breakpoint. */
extern uint32 status_reg;
/* Hold copy of frame length of frame received (if good), so reader can examine it at a breakpoint. */
extern uint16 frame_len ;
extern void BPhero_UWB_Message_Init(void);
extern void BPhero_UWB_Init(void);//dwm1000 init related

#endif
